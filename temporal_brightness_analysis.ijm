//rename original image
run("Duplicate...", "title=[2021-03-02-beta2tg-500nMJE1319-100nMCGP-day1-series1] duplicate ");
title=getTitle();
run("32-bit");
run("Z Project...", "projection=[Average Intensity]");
//run("Duplicate...", "title=[Mean Intensity "+title+"] duplicate ");
meanI=getTitle();
//selectWindow("[Mean Intensity "+name+"]");
run("Duplicate...", "title=[Squared Mean Intensity "+title+"] ");
SmeanI=getTitle();
run("Square");
//rename("Squared Mean Intensity" + title);
selectWindow(title);
run("Z Project...", "projection=[Standard Deviation]");
run("Square");
rename("Variance "+title);
varia=getTitle();
imageCalculator("Divide create 32-bit", SmeanI,varia);
rename("N "+title);
Ntitle=getTitle();
imageCalculator("Divide create 32-bit", varia,meanI);
rename("B "+title);
Btitle=getTitle();
selectWindow(Ntitle);
run("Enhance Contrast...", "saturated=0.5");
selectWindow(Btitle);
run("Enhance Contrast...", "saturated=0.5");
selectWindow(SmeanI);
run("Enhance Contrast...", "saturated=0.5");
selectWindow(varia);
run("Enhance Contrast...", "saturated=0.5");


//selectWindow(Ntitle);
//run("Histogram", "bins=256 use x_min=0.01 x_max=4.61 y_max=Auto");
//selectWindow(Btitle);
//run("Histogram", "bins=256 use x_min=0.51 x_max=1.46 y_max=Auto");